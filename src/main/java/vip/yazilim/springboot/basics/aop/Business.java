package vip.yazilim.springboot.basics.aop;

import org.springframework.stereotype.Component;

/**
 * Simple Business object
 * 
 * @author Emre Sen
 * 
 */
@Component
public class Business {

	public void getARecord() {
		System.out.println(this.getClass().getName());
	}

	public void wrong() throws Exception {
		throw new Exception();
	}

}
