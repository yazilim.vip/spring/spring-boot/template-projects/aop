package vip.yazilim.springboot.basics.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.Configuration;

/**
 * Aspects are defined here.
 * 
 * @author Emre Sen
 *
 */
@Configuration
@Aspect
public class BusinessAspect {

	private static final String ASPECT_PACKAGE_PATTERN = "execution(* vip.yazilim.springboot.basics.aop.*.*(..))";

	/**
	 * to be executed before invoking methods which matches given pattern before
	 * executed
	 * 
	 * @param jpoint point where aspect joins
	 */
	@Before(ASPECT_PACKAGE_PATTERN)
	public void before(JoinPoint jpoint) {
		System.out.println("\nBefore called => " + jpoint);
	}

	/**
	 * to be executed before invoking methods which matches given pattern after
	 * executed
	 * 
	 * @param jpoint point where aspect joins
	 */
	@After(ASPECT_PACKAGE_PATTERN)
	public void after(JoinPoint jpoint) {
		System.out.println("After called => " + jpoint);
	}

	/**
	 * to be executed before invoking methods which matches given pattern returns
	 * 
	 * @param jpoint point where aspect joins
	 */
	@AfterReturning(ASPECT_PACKAGE_PATTERN)
	public void afterReturning(JoinPoint jpoint) {
		System.out.println("After returning... => " + jpoint);
	}

	/**
	 * to be executed before invoking methods which matches given pattern throws
	 * exception
	 * 
	 * @param jpoint point where aspect joins
	 */
	@AfterThrowing(ASPECT_PACKAGE_PATTERN)
	public void afterThrowing(JoinPoint jpoint) {
		System.out.println("After Throwing Exception ... => " + jpoint);
	}

	/**
	 * ..
	 * @param pjpoint point where aspect joins
	 * @throws Throwable
	 */
	@Around(ASPECT_PACKAGE_PATTERN)
	public void around(ProceedingJoinPoint pjpoint) throws Throwable {

		long start = System.currentTimeMillis();
		pjpoint.proceed();
		long end = System.currentTimeMillis();
		System.out.println("It took : " + (end - start) + " ms");
	}

}
